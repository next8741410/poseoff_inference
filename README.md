## Pose1:

![pose1.jpg](pose_images/pose1.jpg)

## Pose2:
![pose1.jpg](pose_images/pose2.png)


## Pose3:
![pose3.png](pose_images/pose3.png)


## Pose4:
![pose4.png](pose_images/pose4.png)

## Pose5:
![pose5.jpg](pose_images/pose5.jpg)

## Pose6
![pose6.png](pose_images/pose6.png)

## Pose7
![pose7.png](pose_images/pose7.png)

## Pose8
![pose8.jpeg](pose_images/pose8.jpeg)

## Pose9
![pose9.png](pose_images/pose9.png)

## Pose10

![pose10.png](pose_images/pose10.png)


## installation
1. clone the repo 

```
git clone git@bitbucket.org:audaxlabs/poseoff.git
```
2. install python>3.8
3. create and activate a python environment and install requirements
```bazaar
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
4. run app
```bazaar
python main.py
```

## install using Docker 
install docker than run below command
```bazaar
docker build -t poseoff .
docker run -d -p 9002:9002
```


