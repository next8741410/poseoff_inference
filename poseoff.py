import math
import cv2
import mediapipe as mp
from mediapipe.tasks import python
from mediapipe.tasks.python import vision
from config import HandRecoConfig, ImagePath, PoseName
import numpy as np


class PoseDetection:
    def __init__(self):
        self.mp_pose = mp.solutions.pose
        self.pose = self.mp_pose.Pose(static_image_mode=True, min_detection_confidence=0.3,
                                      model_complexity=2)
        self.mp_drawing = mp.solutions.drawing_utils
        self.base_options = python.BaseOptions(model_asset_path=HandRecoConfig.gesture_model)
        self.options = vision.GestureRecognizerOptions(base_options=self.base_options)
        self.recognizer = vision.GestureRecognizer.create_from_options(self.options)
    
    def hand_recogniger(self, image):
        top_gesture = "not recog"
        image = mp.Image.create_from_file(image)
        
        # STEP 4: Recognize gestures in the input image.
        recognition_result = self.recognizer.recognize(image)
        
        # STEP 5: Process the result. In this case, visualize it.
        if len(recognition_result.gestures) > 0:
            top_gesture = recognition_result.gestures[0][0].category_name
            
            print(top_gesture)
        # hand_landmarks = recognition_result.hand_landmarks
        return top_gesture
    
    def calculateAngle(self, landmark1, landmark2, landmark3):
        '''
        This function calculates angle between three different landmarks.
        Args:
            landmark1: The first landmark containing the x,y and z coordinates.
            landmark2: The second landmark containing the x,y and z coordinates.
            landmark3: The third landmark containing the x,y and z coordinates.
        Returns:
            angle: The calculated angle between the three landmarks.

        '''
        # Get the required landmarks coordinates.
        x1, y1, _ = landmark1
        x2, y2, _ = landmark2
        x3, y3, _ = landmark3
        
        # Calculate the angle between the three points
        angle = math.degrees(math.atan2(y3 - y2, x3 - x2) - math.atan2(y1 - y2, x1 - x2))
        
        # Check if the angle is less than zero.
        if angle < 0:
            # Add 360 to the found angle.
            angle += 360
        
        # Return the calculated angle.
        return angle
    
    def is_Pose9(self, left_elbow_angle, right_elbow_angle, left_shoulder_angle, right_shoulder_angle,
                 left_knee_angle, right_knee_angle):
        label = False
        # set elbow angle
        if (200 < left_elbow_angle < 300 and 300 < right_elbow_angle < 350) or (
            200 < right_elbow_angle < 320 and 300 < left_elbow_angle < 350):
            # set shoulder angle
            if (20 < left_shoulder_angle < 80 and 20 < right_shoulder_angle < 80):
                # set knee angle
                if (150 < left_knee_angle < 200 and 150 < right_knee_angle < 200):
                    label = True
        return label
    
    def is_Pose2(self, left_elbow_angle, right_elbow_angle, left_shoulder_angle, right_shoulder_angle,
                 left_knee_angle, right_knee_angle):
        label = False
        if (230 < left_elbow_angle < 290 and 80 < right_elbow_angle < 135) or (
            230 < right_elbow_angle < 300 and 80 < left_elbow_angle < 135):
            # set shoulder angle
            if (20 < left_shoulder_angle < 80 and 20 < right_shoulder_angle < 80):
                # set knee angle
                if (165 < left_knee_angle < 200 and 160 < right_knee_angle < 200):
                    label = True
        
        return label
    
    def is_Pose10(self, left_elbow_angle, right_elbow_angle, left_shoulder_angle, right_shoulder_angle,
                 left_knee_angle, right_knee_angle):
        label = False
        
        if (30 < left_elbow_angle < 110 and 230 < right_elbow_angle < 320) or (
            20 < right_elbow_angle < 70 and 230 < left_elbow_angle < 280):
            if (0 < left_shoulder_angle < 50 and 90 < right_shoulder_angle < 140) or (
                    0 < right_shoulder_angle < 50 and 90 < left_shoulder_angle < 140):
                if 130 < left_knee_angle < 230 and 130 < right_knee_angle < 230:
                    label = True
        return label
    
    def is_Pose4(self, left_elbow_angle, right_elbow_angle, left_shoulder_angle, right_shoulder_angle,
                 left_knee_angle, right_knee_angle):
        label = False
        if (270 < left_elbow_angle < 340 and 60 < right_elbow_angle < 120) or (
            280 < right_elbow_angle < 340 and 60 < left_elbow_angle < 120):
            # set shoulder angle
            if (5 < left_shoulder_angle < 50 and 0 < right_shoulder_angle < 40):
                # set knee angle
                if (150 < left_knee_angle < 210 and 150 < right_knee_angle < 210):
                    label = True
        return label
    
    def is_Pose5(self, left_elbow_angle, right_elbow_angle, left_shoulder_angle, right_shoulder_angle,
                 left_knee_angle, right_knee_angle):
        label = False
        # cv2.imwrite("a.png", output_image)
        pose_name = self.hand_recogniger(ImagePath.image_value)
        # print("pose_name", pose_name)
        if pose_name == PoseName.pose5:
            label = True
        return label
    
    def is_Pose6(self, left_elbow_angle, right_elbow_angle, left_shoulder_angle, right_shoulder_angle,
                 left_knee_angle, right_knee_angle):
        label = False
        pose_name = self.hand_recogniger(ImagePath.image_value)
        print("pose_name", pose_name)
        if pose_name == PoseName.pose6:
            label = True
        return label
    
    def is_Pose7(self, left_elbow_angle, right_elbow_angle, left_shoulder_angle, right_shoulder_angle,
                 left_knee_angle, right_knee_angle):
        label = False
        if (40 < left_elbow_angle < 90 and 100 < right_elbow_angle < 150) or (
            40 < right_elbow_angle < 90 and 100 < left_elbow_angle < 150) or (
            200 < left_elbow_angle < 260 and 230 < right_elbow_angle < 290) or (
            200 < right_elbow_angle < 260 and 230 < left_elbow_angle < 290):
            # set shoulder angle
            if (30 < left_shoulder_angle < 90 and 30 < right_shoulder_angle < 90):
                # set knee angle
                if 160 < left_knee_angle < 200 and 160 < right_knee_angle < 200:
                    label = True
        return label
    
    def is_Pose8(self, left_elbow_angle, right_elbow_angle, left_shoulder_angle, right_shoulder_angle,
                 left_knee_angle, right_knee_angle):
        label = False
        if (100 < left_elbow_angle < 150 and 200 < right_elbow_angle < 250) or (
            100 < right_elbow_angle < 150 and 200 < left_elbow_angle < 250):
            # set shoulder angle
            if 90 < left_shoulder_angle < 160 and 90 < right_shoulder_angle < 160:
                label = True
        return label
    
    def is_Pose1(self, left_elbow_angle, right_elbow_angle, left_shoulder_angle, right_shoulder_angle,
                 left_knee_angle, right_knee_angle):
        label = False
        if (150 < left_elbow_angle < 200 and 8 < right_elbow_angle < 70) or (
            150 < right_elbow_angle < 200 and 8 < left_elbow_angle < 70) or (
            300 < left_elbow_angle < 350 and 150 < right_elbow_angle < 200) or (
                150 < right_elbow_angle < 200 and 300 < left_elbow_angle < 350):
            # set shoulder angle
            if (35 < right_shoulder_angle < 90 or 35 < left_shoulder_angle < 90):
                # set knee angle
                
                label = True
        return label
    
    def is_Pose3(self, left_elbow_angle, right_elbow_angle, left_shoulder_angle, right_shoulder_angle,
                  left_knee_angle, right_knee_angle):
        label = False
        if (50 < left_elbow_angle < 90 and 280 < right_elbow_angle < 320) or (
            280 < right_elbow_angle < 320 and 50 < left_elbow_angle < 90):
            # set shoulder angle
            if 50 < left_shoulder_angle < 100 and 50 < right_shoulder_angle < 100:
                # set knee angle
                label = True
        return label
    
    def classifyPose(self, landmarks, output_image: np.ndarray, pose_name: str = None):
        """
        This function classifies yoga poses depending upon the angles of various body joints.
        Args:
            landmarks: A list of detected landmarks of the person whose pose needs to be classified.
            output_image: A image of the person with the detected pose landmarks drawn.
            display: A boolean value that is if set to true the function displays the resultant image with the pose label
            written on it and returns nothing.
        Returns:
            output_image: The image with the detected pose landmarks drawn and pose label written.
            label: The classified pose label of the person in the output_image.

        """
        
        # Initialize the label of the pose. It is not known at this stage.
        Result = False
        
        # Specify the color (Red) with which the label will be written on the image.
        # color = (0 , 0 , 255)
        
        # Calculate the required angles.
        # ----------------------------------------------------------------------------------------------------------------
        
        # Get the angle between the left shoulder, elbow and wrist points.
        left_elbow_angle = self.calculateAngle(landmarks[self.mp_pose.PoseLandmark.LEFT_SHOULDER.value],
                                               landmarks[self.mp_pose.PoseLandmark.LEFT_ELBOW.value],
                                               landmarks[self.mp_pose.PoseLandmark.LEFT_WRIST.value])
        
        # Get the angle between the right shoulder, elbow and wrist points.
        right_elbow_angle = self.calculateAngle(landmarks[self.mp_pose.PoseLandmark.RIGHT_SHOULDER.value],
                                                landmarks[self.mp_pose.PoseLandmark.RIGHT_ELBOW.value],
                                                landmarks[self.mp_pose.PoseLandmark.RIGHT_WRIST.value])
        
        # Get the angle between the left elbow, shoulder and hip points.
        left_shoulder_angle = self.calculateAngle(landmarks[self.mp_pose.PoseLandmark.LEFT_ELBOW.value],
                                                  landmarks[self.mp_pose.PoseLandmark.LEFT_SHOULDER.value],
                                                  landmarks[self.mp_pose.PoseLandmark.LEFT_HIP.value])
        
        # Get the angle between the right hip, shoulder and elbow points.
        right_shoulder_angle = self.calculateAngle(landmarks[self.mp_pose.PoseLandmark.RIGHT_HIP.value],
                                                   landmarks[self.mp_pose.PoseLandmark.RIGHT_SHOULDER.value],
                                                   landmarks[self.mp_pose.PoseLandmark.RIGHT_ELBOW.value])
        
        # Get the angle between the left hip, knee and ankle points.
        left_knee_angle = self.calculateAngle(landmarks[self.mp_pose.PoseLandmark.LEFT_HIP.value],
                                              landmarks[self.mp_pose.PoseLandmark.LEFT_KNEE.value],
                                              landmarks[self.mp_pose.PoseLandmark.LEFT_ANKLE.value])
        
        # Get the angle between the right hip, knee and ankle points
        right_knee_angle = self.calculateAngle(landmarks[self.mp_pose.PoseLandmark.RIGHT_HIP.value],
                                               landmarks[self.mp_pose.PoseLandmark.RIGHT_KNEE.value],
                                               landmarks[self.mp_pose.PoseLandmark.RIGHT_ANKLE.value])
        
        # ----------------------------------------------------------------------------------------------------------------
        print(left_elbow_angle, right_elbow_angle, left_shoulder_angle, right_shoulder_angle, left_knee_angle,
              right_knee_angle)
        Result = getattr(self, f"is_{pose_name}")(left_elbow_angle, right_elbow_angle, left_shoulder_angle,
                                                  right_shoulder_angle, left_knee_angle, right_knee_angle)
        
        return output_image, Result
    
    def detectPose(self, image):
        """
        This function performs pose detection on an image.
        Args:
            image: The input image with a prominent person whose pose landmarks needs to be detected.
 
        Returns:
            output_image: The input image with the detected pose landmarks drawn.
            landmarks: A list of detected landmarks converted into their original scale.
        """
        
        # Create a copy of the input image.
        output_image = image.copy()
        
        # Convert the image from BGR into RGB format.
        imageRGB = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        
        # Perform the Pose Detection.
        results = self.pose.process(imageRGB)
        
        # Retrieve the height and width of the input image.
        height, width, _ = image.shape
        
        # Initialize a list to store the detected landmarks.
        landmarks = []
        
        # Check if any landmarks are detected.
        if results.pose_landmarks:
            
            # Draw Pose landmarks on the output image.
            self.mp_drawing.draw_landmarks(image=output_image, landmark_list=results.pose_landmarks,
                                           connections=self.mp_pose.POSE_CONNECTIONS)
            
            # Iterate over the detected landmarks.
            for landmark in results.pose_landmarks.landmark:
                # Append the landmark into the list.
                landmarks.append((int(landmark.x * width), int(landmark.y * height),
                                  (landmark.z * width)))
        # Return the output image and the found landmarks.
        return output_image, landmarks


if __name__ == "main":
    pass
