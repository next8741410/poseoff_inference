# STEP 1: Import the necessary modules.
import mediapipe as mp
from mediapipe.tasks import python
from mediapipe.tasks.python import vision

# STEP 2: Create an GestureRecognizer object.
base_options = python.BaseOptions(model_asset_path='/home/ajaytiwari/Downloads/gesture_recognizer.task')
options = vision.GestureRecognizerOptions(base_options=base_options)
recognizer = vision.GestureRecognizer.create_from_options(options)

images = []
results = []

  # STEP 3: Load the input image.
image_file_name="/home/ajaytiwari/Documents/poseoff/uploads/2024-01-29-132803.jpg"
image = mp.Image.create_from_file(image_file_name)

# STEP 4: Recognize gestures in the input image.
recognition_result = recognizer.recognize(image)

# STEP 5: Process the result. In this case, visualize it.

top_gesture = recognition_result.gestures[0][0]
hand_landmarks = recognition_result.hand_landmarks
print(top_gesture)