from pathlib import Path
from sys import path
from fastapi import FastAPI, UploadFile, Form
from pydantic import BaseModel, FilePath
import uvicorn
import cv2
from poseoff import PoseDetection
from PIL import Image
from io import BytesIO
from base64 import b64decode
from fastapi.middleware.cors import CORSMiddleware
from config import ImagePath

app = FastAPI()
pose_obj = PoseDetection()

upload_dir = Path("uploads")
origins = ["*"]

app.add_middleware(
	CORSMiddleware,
	allow_origins=origins,
	allow_credentials=True,
	allow_methods=["*"],
	allow_headers=["*"],
)


class ResultModel(BaseModel):
	detection: bool


@app.post("/classify_pose", response_model=ResultModel)
async def classify_pose_endpoint(pose_name: str = Form(...), filedata: str = Form(...)):
	try:
		label = False
		im = Image.open(BytesIO(b64decode(filedata.split(',')[1])))
		im.save(ImagePath.image_value)
		img_arr = cv2.imread(str(ImagePath.image_value))
		out, landmark = pose_obj.detectPose(img_arr)
		if landmark:
			output_image, label = pose_obj.classifyPose(landmark, out, pose_name)

		return {"detection": label}
	except Exception as e:
		print('error', str(e))
		return {"detection": False}


@app.post("/test")
async def classify_pose_endpoint1(file: UploadFile, pose_name: str):
	try:
		result = False
		# print(pose_name)
		file_data = await file.read()
		upload_dir.mkdir(parents=True, exist_ok=True)
		file_path = upload_dir / file.filename
		with file_path.open("wb") as f:
			f.write(file_data)
		img_arr = cv2.imread(str(file_path))
		print(file_path)
		out, landmark = pose_obj.detectPose(img_arr)
		# print("asd",landmark)
		if landmark:
			output_image, label = pose_obj.classifyPose(landmark, out, pose_name)
			result = label
		# print(label)
		# print(out)
		return {"detection": result}
	except Exception as e:
		print('error', str(e))
		return {"detection": False}


if __name__ == "__main__":
	uvicorn.run('app:app', host='0.0.0.0', port=9002, reload=True)
